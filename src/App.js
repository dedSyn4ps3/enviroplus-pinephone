import * as React from "react";

import PropTypes from 'prop-types';
import { useTheme } from '@mui/material/styles';
import AppBar from '@mui/material/AppBar';
import Tabs from '@mui/material/Tabs';
import Tab from '@mui/material/Tab';
import Typography from '@mui/material/Typography';
import Box from '@mui/material/Box';

import AppSettingsAltIcon from '@mui/icons-material/AppSettingsAlt';
import InfoOutlinedIcon from '@mui/icons-material/InfoOutlined';
import HomeRoundedIcon from '@mui/icons-material/HomeRounded';
import ReadMoreIcon from '@mui/icons-material/ReadMore';

import Primary from './Primary';
import Settings from "./Settings";
import About from './About';
import More from "./MoreProjects";


function TabPanel(props) {
  const { children, value, index, ...other } = props;

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`full-width-tabpanel-${index}`}
      aria-labelledby={`full-width-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

function a11yProps(index) {
  return {
    id: `full-width-tab-${index}`,
    'aria-controls': `full-width-tabpanel-${index}`,
  };
}

export default function FullWidthTabs() {
  const theme = useTheme();
  const [value, setValue] = React.useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <Box sx={{ bgcolor: 'background.paper', marginBottom: 20 }}>
      <AppBar position="sticky">
        <Tabs
          value={value}
          onChange={handleChange}
          indicatorColor="secondary"
          textColor="inherit"
	      variant="fullWidth"
          aria-label="full width tabs example"
        >
          <Tab icon={<HomeRoundedIcon />} label="OVERVIEW" {...a11yProps(0)} />
          <Tab icon={<AppSettingsAltIcon />} label="SETTINGS" {...a11yProps(1)} />
          <Tab icon={<InfoOutlinedIcon />} label="ABOUT" {...a11yProps(2)} />
          <Tab icon={<ReadMoreIcon />} label="MORE" {...a11yProps(3)} />
        </Tabs>
      </AppBar>
        <TabPanel value={value} index={0} dir={theme.direction}>
          <Primary />
        </TabPanel>
        <TabPanel value={value} index={1} dir={theme.direction}>
          <Settings />
        </TabPanel>
        <TabPanel value={value} index={2} dir={theme.direction}>
          <About />
        </TabPanel>
        <TabPanel value={value} index={3} dir={theme.direction}>
          <More />
        </TabPanel>
    </Box>
  );
}
