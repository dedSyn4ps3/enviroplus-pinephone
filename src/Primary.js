import React, { Component } from "react";
import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  Row,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Label
} from "reactstrap";

import CircularProgressWithLabel from './CircularProgress'
import { invoke } from '@tauri-apps/api/tauri'

class Primary extends Component {

  intervalID;

  constructor(props) {
    super(props);
    this.state = {
      temp: 0, hum: 0, press: 0,
      co: 0, nh3: 0,
      addressModalVisible: false, errorModalVisible: false
    }
    this.callBackend = this.callBackend.bind(this);
  }

  componentDidMount() {
    this.callBackend();
  }

  componentWillUnmount() {
    clearTimeout(this.intervalID);
  }


  setAddressModalVisible = (visible) => {
    this.setState({ addressModalVisible: visible });
  }

  setErrorModalVisible = (visible) => {
    this.setState({ errorModalVisible: visible });
  }

  //////////////////////////////////////////////////////////////////////////////////
  //  Make call to Rust backend to query the Pi's flask server for sensor values  //
  //////////////////////////////////////////////////////////////////////////////////

  callBackend() {
    let i = localStorage.getItem("pi_address");
    if (i === null) {
      console.log("[!] No sensor address available -> Update device info using the SideDash tab... [!]");
      this.setAddressModalVisible(true);
    } else if (i == "") {
      console.log("[!] No sensor address available -> Update device info using the SideDash tab... [!]");
      this.setAddressModalVisible(true);
    } else {
      invoke('get_data', { address: i.replace(/"/g, ''), endpoint: 'temperature' }).then(value => { this.setState({ temp: parseFloat(value) }) }).catch((e) => console.error(e));
      invoke('get_data', { address: i.replace(/"/g, ''), endpoint: 'humidity' }).then(value => { this.setState({ hum: parseFloat(value) }) }).catch((e) => console.error(e));
      invoke('get_data', { address: i.replace(/"/g, ''), endpoint: 'reducing' }).then(value => { this.setState({ co: parseFloat(value) }) }).catch((e) => console.error(e));
      invoke('get_data', { address: i.replace(/"/g, ''), endpoint: 'ammonia' }).then(value => { this.setState({ nh3: parseFloat(value) }) }).catch((e) => console.error(e));
      invoke('get_data', { address: i.replace(/"/g, ''), endpoint: 'pressure' }).then(value => { this.setState({ press: parseFloat(value) }) }).catch((e) => { console.error(e); this.setErrorModalVisible(true) });
      this.intervalID = setTimeout(this.callBackend.bind(this), 60000);
    }
  }

  render() {
    return (
      <>
        <ErrorModal />
        <WarningModal />
        <Row style={{ marginBottom: 20 }}>
          <Card className="card-chart h-100" style={{ marginLeft: 3, marginBottom: 20, maxWidth: 325, height: 400 }}>
            <CardHeader>
              <h5 className="card-category">Sensor Data</h5>
              <CardTitle tag="h4">Temperature</CardTitle>
            </CardHeader>
            <CardBody style={{ marginLeft: '-25%', marginBottom: 20 }}>
              <div className="chart-area">
                <CircularProgressWithLabel value={this.state.temp} symbol="°F" style={{ width: 200, height: 200 }} color="success" />
              </div>
            </CardBody>
          </Card>
        </Row>
        <Row>
          <Card className="card-chart h-100" style={{ marginLeft: 3, marginBottom: 20, maxWidth: 325, height: 400 }}>
            <CardHeader>
              <h5 className="card-category">Sensor Data</h5>
              <CardTitle tag="h4">Humidity</CardTitle>
            </CardHeader>
            <CardBody style={{ marginLeft: '-25%', marginBottom: 20 }}>
              <div className="chart-area">
                <CircularProgressWithLabel value={this.state.hum} symbol="%" style={{ width: 200, height: 200 }} color="info" />
              </div>
            </CardBody>
          </Card>
        </Row>
        <Row>
          <Card className="card-chart h-100" style={{ marginLeft: 3, maxWidth: 325, height: 400 }}>
            <CardHeader>
              <h5 className="card-category">Sensor Data</h5>
              <CardTitle tag="h4">Pressure</CardTitle>
            </CardHeader>
            <CardBody style={{ marginLeft: '-25%', marginBottom: 20 }}>
              <div className="chart-area">
                <Label style={{ fontSize: 42, marginTop: 40 }}>{this.state.press} hPa</Label>
              </div>
            </CardBody>
          </Card>
        </Row>
        <Row style={{ marginBottom: 20 }}>
          <Card className="card-chart h-100" style={{ marginLeft: 3, marginBottom: 20, maxWidth: 325, height: 400 }}>
            <CardHeader>
              <h5 className="card-category">(CO)</h5>
              <CardTitle tag="h4">Reducing</CardTitle>
            </CardHeader>
            <CardBody style={{ marginLeft: '-25%', marginBottom: 20 }}>
              <div className="chart-area">
                <Label style={{ fontSize: 42, marginTop: 40 }}>{this.state.co} kO</Label>
              </div>
            </CardBody>
          </Card>
        </Row>
        <Row>
          <Card className="card-chart h-100" style={{ marginLeft: 3, marginBottom: 20, maxWidth: 325, height: 400 }}>
            <CardHeader>
              <h5 className="card-category">(NH3)</h5>
              <CardTitle tag="h4">Ammonia</CardTitle>
            </CardHeader>
            <CardBody style={{ marginLeft: '-25%', marginBottom: 20 }}>
              <div className="chart-area">
                <Label style={{ fontSize: 42, marginTop: 40 }}>{this.state.nh3} kO</Label>
              </div>
            </CardBody>
          </Card>
        </Row>
      </>
    );
  }
}

export default Primary;

///////////////////////////////////////////////////////////////
//  Warning modal in case no Pi address stored for requests  //
///////////////////////////////////////////////////////////////

class WarningModal extends Primary {
  render() {
    const { addressModalVisible } = this.state;
    return (
      <div>
        <Modal isOpen={addressModalVisible} style={{ width: 315, marginTop: 80, marginLeft: 25 }} toggle={() => { this.setAddressModalVisible(!addressModalVisible); }}>
          <ModalHeader>Empty Pi Address</ModalHeader>
          <ModalBody>
            You currently do not have a backend Pi address stored to query sensor data
            from! <b>To fix this issue, make sure to click on the Settings tab and input
              the ip address (including the port number) of the device running the
              sensor backend server.</b>
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={() => { this.setAddressModalVisible(!addressModalVisible); }}>Dismiss</Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

///////////////////////////////////////////
//  Error modal in case requests fail    //
///////////////////////////////////////////

class ErrorModal extends Primary {
  render() {
    const { errorModalVisible } = this.state;
    return (
      <div>
        <Modal isOpen={errorModalVisible} style={{ width: 315, marginTop: 80, marginLeft: 25 }} toggle={() => { this.setErrorModalVisible(!errorModalVisible); }}>
          <ModalHeader>Sensor Request Error</ModalHeader>
          <ModalBody>
            An error occurred while attempting to retrieve values from the backend
            Raspberry Pi server. This is most likely due to the querying of an
            improper backend address. <b>Check your local network to see if your Pi has been
              assigned a new ip address</b>, then update it accordingly from the Settings tab.
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={() => { this.setErrorModalVisible(!errorModalVisible); }}>Dismiss</Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}
