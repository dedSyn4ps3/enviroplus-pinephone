import React, { Component } from "react";
import {
    Button,
    Card,
    CardHeader,
    CardBody,
    FormGroup,
    Form,
    Input,
    Row,
    Col,
} from "reactstrap";


class Settings extends Component {
    constructor(props) {
        super(props);
        this.state = {
            pi_address: ""
        }
        this.handleSubmit = this.handleSubmit.bind(this);
        this.saveValues = this.saveValues.bind(this);
    }

    componentDidMount() {
        this.loadValues();
    }

    handleChange = (prop) => (event) => {
        this.setState({ ...this.state, [prop]: event.target.value });
    };

    saveValues = () => {
        localStorage.setItem("co_feed_title", JSON.stringify(this.state.secondary_feed1_title));
        localStorage.setItem("nh3_feed_title", JSON.stringify(this.state.secondary_feed2_title));
        localStorage.setItem("pi_address", JSON.stringify(this.state.pi_address));
    }

    loadValues = () => {

        let i = localStorage.getItem("pi_address")

        if (i === null) {
            this.setState({ pi_address: "" });
        } else {
            this.setState({ pi_address: JSON.parse(i) });
        }
    }

    handleSubmit() {
        this.saveValues()
        this.refreshPage()
    }

    refreshPage() {
        window.location.reload(false);
    }

    render() {
        return (
            <>
                <div className="content">
                    <Row>
                        <Col xs={12}>
                            <Card style={{ height: 450, marginLeft: -6, width: 325 }}>
                                <CardHeader style={{ marginTop: 20 }}>
                                    <h5 className="title">Current Configuration</h5>
                                </CardHeader>
                                <CardBody style={{ marginTop: 30 }}>
                                    <Form>
                                        <Row style={{ marginLeft: 10 }}>
                                            <Col className="pr-1" md="5">
                                                <FormGroup>
                                                    <label>Raspberry Pi Server Address</label>
                                                    <Input
                                                        value={this.state.pi_address}
                                                        onChange={this.handleChange('pi_address')}
                                                        placeholder="<ip_address:port>"
                                                        type="text"
                                                    />
                                                </FormGroup>
                                            </Col>
                                        </Row>
                                        <Row>
                                            <Col style={{ marginLeft: '32%', marginTop: '2%' }}>
                                                <FormGroup>
                                                    <Button color="success" onClick={this.handleSubmit}>Update</Button>
                                                </FormGroup>
                                            </Col>
                                        </Row>
                                    </Form>
                                </CardBody>
                            </Card>
                        </Col>
                    </Row>
                </div>
            </>
        );
    }
}

export default Settings;
