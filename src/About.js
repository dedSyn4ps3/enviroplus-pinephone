/*!
=========================================================
        ** Enviro+ Pinephone Dashboard **
=========================================================

MIT License

Copyright (c) 2022 dedSyn4ps3

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in all
copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
SOFTWARE.

***This project is built on a skeleton using components designed by Creative Tim***

Any use, re-use, and adaptations of this project and it's code shall include the above license as well
as credit for the components used that were adapted from Creative Tim's work...

* The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
*/


import React from "react";

import {
    Card,
    CardHeader,
    CardBody,
    Row,
    Col,
} from "reactstrap";

function About() {
    return (
        <>
            <div className="content">
                <Row>
                    <Col>
                        <Card>
                            <CardHeader>
                                <h3 className="title" style={{ marginTop: 10 }}>Flexibility Is Key</h3>
                            </CardHeader>
                            <CardBody>
                                <p>
                                    The goal of this project, like it's sibling, is to offer another exciting way to utilize the excellent Enviro Plus HAT from Pimoroni. 
                                    Unlike the prior versions of this project, this one has been tailored to run on the Pine64 PinePhone!
                                </p>
                                <p>
                                    It's important to remember that even though the code for this project was written primarily for, and tested on the PinePhone, <b>it 
                                    by no means should be limited to just that device.</b> The code here provides the foundation, allowing for adjustments to be made for 
                                    other Linux phones, as well as development of new applications using similar framework stacks.
                                </p>
                            </CardBody>
                            <CardHeader>
                                <h4 className="title">Building Blocks</h4>
                            </CardHeader>
                            <CardBody>
                                <p>
                                    The core framework used in this project is React. Like other frameworks, it offers flexible components and structures to build both simple and complex web apps.
                                    This project is merely one of thousands of React-based applications, and will hopefully serve as a launchpad for makers and others to
                                    discover new ways to expand their current projects, and maybe even inspire new ones...
                                </p>
                                <p>
                                    <b>React may be the face of this app, but Rust orchestrates it!.</b> The Tauri framework offers versatility similar to that of Electron 
                                    in terms of allowing developers to create modern, efficient desktop applications. It achieves this by using the Rust language for backend 
                                    code handling and system API calls. <b>The goal here was to show that a framework with the power to create awesome desktop apps can do the same 
                                    for mobile devices running Linux such as the PinePhone!</b>
                                </p>
                            </CardBody>
                        </Card>
                    </Col>
                </Row>
            </div>
        </>
    );
}

export default About;
