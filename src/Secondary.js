import React, { Component } from "react";
import {
  Card,
  CardHeader,
  CardBody,
  CardTitle,
  Row,
  Button,
  Modal,
  ModalHeader,
  ModalBody,
  ModalFooter,
  Label
} from "reactstrap";

import { invoke } from '@tauri-apps/api/tauri'


export default class Secondary extends Component {

  refreshID;

  constructor(props) {
    super(props);
    this.state = {
      co_feed_title: "",
      nh3_feed_title: "",
      co: 0,
      nh3: 0,
      addressModalVisible: false, errorModalVisible: false
    }
    this.getOtherData = this.getOtherData.bind(this);
  }

  componentDidMount() {
    this.loadValues();
    this.getOtherData();
  }

  componentWillUnmount() {
    clearTimeout(this.refreshID);
  }

  setAddressModalVisible = (visible) => {
    this.setState({ addressModalVisible: visible });
  }

  setErrorModalVisible = (visible) => {
    this.setState({ errorModalVisible: visible });
  }

  getOtherData() {
    let i = localStorage.getItem("pi_address");
    if (i === null) {
      console.log("[!] No sensor address available -> Update device info using the SideDash tab... [!]");
      this.setAddressModalVisible(true);
    } else if (i == "") {
      console.log("[!] No sensor address available -> Update device info using the SideDash tab... [!]");
      this.setAddressModalVisible(true);
    } else {
      invoke('get_data', { address: i.replace(/"/g, ''), endpoint: 'reducing' }).then(value => { this.setState({ co: parseFloat(value) }) }).catch((e) => console.error(e));
      invoke('get_data', { address: i.replace(/"/g, ''), endpoint: 'ammonia' }).then(value => { this.setState({ nh3: parseFloat(value) }) }).catch((e) => { console.error(e); this.setErrorModalVisible(true) });
      this.refreshID = setTimeout(this.getOtherData.bind(this), 60000);
    }
  }

  loadValues = () => {
    let i = localStorage.getItem("co_feed_title")

    if (i === null) {
      this.setState({ co_feed_title: "Reducing" });
    } else {
      this.setState({ co_feed_title: JSON.parse(i) });
    }

    i = localStorage.getItem("nh3_feed_title")

    if (i === null) {
      this.setState({ nh3_feed_title: "Ammonia" });
    } else {
      this.setState({ nh3_feed_title: JSON.parse(i) });
    }
  }

  render() {
    return (
      <>
        <ErrorModal />
        <WarningModal />
        <Row style={{ marginBottom: 20 }}>
          <Card className="card-chart h-100" style={{ marginLeft: 3, marginBottom: 20, maxWidth: 325, height: 400 }}>
            <CardHeader>
              <h5 className="card-category">Secondary Feed (CO)</h5>
              <CardTitle tag="h4">{this.state.co_feed_title}</CardTitle>
            </CardHeader>
            <CardBody style={{ marginLeft: '-25%', marginBottom: 20 }}>
              <div className="chart-area">
                <Label style={{ fontSize: 60, marginTop: 20 }}>{this.state.co} kO</Label>
              </div>
            </CardBody>
          </Card>
        </Row>
        <Row>
          <Card className="card-chart h-100" style={{ marginLeft: 3, marginBottom: 20, maxWidth: 325, height: 400 }}>
            <CardHeader>
              <h5 className="card-category">Secondary Feed (NH3)</h5>
              <CardTitle tag="h4">{this.state.nh3_feed_title}</CardTitle>
            </CardHeader>
            <CardBody style={{ marginLeft: '-25%', marginBottom: 20 }}>
              <div className="chart-area">
                <Label style={{ fontSize: 60, marginTop: 20 }}>{this.state.nh3} kO</Label>
              </div>
            </CardBody>
          </Card>
        </Row>
      </>
    );
  }
}

///////////////////////////////////////////////////////////////
//  Warning modal in case no Pi address stored for requests  //
///////////////////////////////////////////////////////////////

class WarningModal extends Secondary {
  render() {
    const { addressModalVisible } = this.state;
    return (
      <div>
        <Modal isOpen={addressModalVisible} style={{ width: 315, marginTop: 80, marginLeft: 25 }} toggle={() => { this.setAddressModalVisible(!addressModalVisible); }}>
          <ModalHeader>Empty Pi Address</ModalHeader>
          <ModalBody>
            You currently do not have a backend Pi address stored to query sensor data
            from! <b>To fix this issue, make sure to click on the Settings tab and input
              the ip address (including the port number) of the device running the
              sensor backend server.</b>
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={() => { this.setAddressModalVisible(!addressModalVisible); }}>Dismiss</Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}

///////////////////////////////////////////
//  Error modal in case requests fail    //
///////////////////////////////////////////

class ErrorModal extends Secondary {
  render() {
    const { errorModalVisible } = this.state;
    return (
      <div>
        <Modal isOpen={errorModalVisible} style={{ width: 315, marginTop: 80, marginLeft: 25 }} toggle={() => { this.setErrorModalVisible(!errorModalVisible); }}>
          <ModalHeader>Sensor Request Error</ModalHeader>
          <ModalBody>
            An error occurred while attempting to retrieve values from the backend
            Raspberry Pi server. This is most likely due to the querying of an
            improper backend address. <b>Check your local network to see if your Pi has been
              assigned a new ip address</b>, then update it accordingly from the Settings tab.
          </ModalBody>
          <ModalFooter>
            <Button color="primary" onClick={() => { this.setErrorModalVisible(!errorModalVisible); }}>Dismiss</Button>
          </ModalFooter>
        </Modal>
      </div>
    );
  }
}
