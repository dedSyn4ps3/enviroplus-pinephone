# Enviroplus Pinephone Dashboard
[![Node Version](https://img.shields.io/badge/nodejs-v18.7.0-success)](https://nodejs.org/en/download/)
[![React Version](https://img.shields.io/badge/react-v18.2.0-informational)](https://reactjs.org/docs/getting-started.html)
[![Rust Version](https://img.shields.io/badge/rust-v1.57-orange)](https://www.rust-lang.org)
[![Tauri Version](https://img.shields.io/badge/tauri-v1.1.1-yellow)](https://www.tauri.app)

<br>

![Main Overview](https://gitlab.com/dedSyn4ps3/enviroplus-pinephone/-/raw/main/screenshots/header.png "A Mobile Linux Project") 

# Overview
The goal of this project, like it's sibling, is to offer another exciting way to utilize the excellent Enviro Plus HAT from Pimoroni. 
Unlike the prior versions of this project, this one has been tailored to run on the Pine64 PinePhone! It's important to remember that 
even though the code for this project was written primarily for, and tested on the PinePhone, <b>it by no means should be limited to 
just this device.</b> The code here provides the foundation, alloadjustments to be made for other Linux phones, as well as development 
of new applications <u>using similar framework stacks</u>.

<br>

## Tailored For Mobile
Unlike the Web and Desktop UI versions of this project, the interface components here have been specifically adjusted and tailored to render 
properly on mobile Linux devices (i.e. PinePhone). The process was made even simpler because of the flexibilty that each framework offers, especially in terms of UI components, which is why these projects have utilized such technologies.

<br>

## Building Blocks

The core framework used in this project is React. Like other frameworks, it offers flexible components and structures to build both simple and complex web apps. This project is merely one of thousands of React-based applications, and will hopefully serve as a lfor makers and others to
discover new ways to expand their current projects, and maybe even inspire new ones...

<b>React may be the face of this app, but Rust orchestrates it!.</b> The Tauri framework offers versatility similar to that of Electron 
in terms of allowing developers to create modern, efficient desktop applications. It achieves this the Rust language for backend 
code handling and system API calls. <b>The goal here was to show that a framework with the power tawesome desktop apps can do the same 
for mobile devices running Linux such as the PinePhone!</b>

<br>

## Quick Start

- Make sure you have already gone through the setup process for the enviroplus-python repo to install the necessary libraries on your Raspberry Pi (**see below**)
- Make sure everything is up and running on your Raspberry Pi!
- Download either the AppImage, binary , or installation file from this repo onto your mobile Linux device
- AppImage:
  ```bash 
  chmod a+x <appimage>
  ./<appimage>
  ``` 
- Deb Installer:
  ```bash 
    sudo dpkg -i <filename.deb>
    ``` 
- Binary:
  ```bash 
      ./<filename>
  ``` 

The preferred method of easily running this app is to utilize the AppImage binary, which has everything required by the app to run packaged in a single file. Most AppImages (including this one) do require a single dependency to be installed named `fuse`. **Simply use your package manager to install this:**

```bash
sudo pacman -S fuse
```
or
```bash
apk add fuse
```

<br>

## Customizations

The code here aims to be a starting point for building other mobile Linux applications using the Tauri framework alongside React (or other frontends). If you plan on testing changes made to this source code, it's imperative that users have `rust` and `cargo` installed on their system. The frontend package manager of choice used here is `yarn`.

In a nutshell: install `rustup` to ensure proper toolchain setup, then run `yarn` from the base of this repo to get all other dependencies installed for development. To run the application in development mode with hot-reloading, simply run `yarn tauri dev`.

**NOTE: It is highly recommended that you conduct any additional testing and development on another, slightly more powerful, ARM device like a Raspberry Pi 4. While it's possible to make code changes and test them locally on the PinePhone, it's much faster and easier to do so on a Pi 4 first. Then after building your new binary, copy it over to your phone using a commandline tool such as `scp`, or a graphical `SFTP` application**

<br>

## Troubleshooting
<u>It's important to keep the following points in mind when using this app</u>:

- If you are running a version of NodeJS > v17.* and want to edit and build the app yourself, you may need to adjust two `package.json` lines to the following because of changes to TLS/SSL implementations:

    - `"start": "cross-env BROWSER=none react-scripts --openssl-legacy-provider start"`

    - `"build": "react-scripts --openssl-legacy-provider build",`

- **The application will immediately close if the Pi server is not running and able to process requests**

- It is recommended that you assign a Static IP Address to your Raspberry Pi to prevent connection issues, should the Pi be assigned a new IP via your local DHCP service

- If your Raspberry Pi happens to be assigned a new IP, the desktop application will no longer launch properly (because it's trying to connect to an invalid address). To fix this issue, simply delete the Local Storage folder on your device to reset the application. Once you do that, the app will launch and prompt you to input the Pi address again


If your Raspberry Pi server address ends up changing for any reason, or you plan on using some other source for retrieving data from, you can easily reset the mobile app by removing local storage data from the following directory:

```bash
rm ~/.local/share/com.enviroplus.pinephone/localstorage/*
```

<br>

## Enviro+ Install Script For Raspberry Pi

```bash
curl -sSL https://get.pimoroni.com/enviroplus | bash
```

### Alternative -> Install and Configure from GitHub

* `git clone https://github.com/pimoroni/enviroplus-python`
* `cd enviroplus-python`
* `sudo ./install.sh`

**Note:** Raspbian/Raspberry Pi OS Lite users may first need to install git: `sudo apt install git`

<br>

## Resources
- Desktop Project - https://gitlab.com/dedSyn4ps3/enviroplus-desktop
- Learning Guides - https://learn.pimoroni.com/
- Other Services - https://www.nullreturn-it.com
- Creative Tim - https://demos.creative-tim.com/

 <h3>Support & Other Questions</h3>

 If you have questions or would like more info on how to set up your project, feel free to [reach out](mailto:support@nullsecurity.tech).

<br>

## Licensing

- Copyright 2022 dedSyn4ps3 (github.com/dedsyn4ps3)
- Licensed under MIT

### This project is built on a skeleton using components designed by Creative Tim

*Any use, re-use, and adaptations of this project and it's code shall include the above license as well as credit for the components used that were adapted from Creative Tim's work...*

- Copyright 2021 Creative Tim (https://www.creative-tim.com)
- Licensed under MIT (https://github.com/creativetimofficial/now-ui-dashboard-react/blob/main/LICENSE.md)
