#![cfg_attr(
  all(not(debug_assertions), target_os = "windows"),
  windows_subsystem = "windows"
)]

use reqwest;
use std::io::Read;

#[tauri::command]
fn get_data(address: String, endpoint: String) -> String {
  let request_url = format!("http://{}/{}", address, endpoint);
  let mut res = reqwest::blocking::get(&request_url).expect("REQUEST FAILED");
  let mut body = String::new();
  res.read_to_string(&mut body).expect("Couldn't read into buffer");
  return body;
}

fn main() {
  tauri::Builder::default()
    .invoke_handler(tauri::generate_handler![get_data])
    .run(tauri::generate_context!())
    .expect("error while running tauri application");
}